from django.conf.urls import patterns, include, url
from django.views.generic.base import RedirectView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout_then_login', name='logout'),

    url(r'^$', RedirectView.as_view(url='orders/stock'), name='main'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^orders/', include('orders.urls')),
)

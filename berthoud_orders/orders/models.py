from django.db import models
from datetime import date


class BaseModel(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class ModelName(BaseModel):
    pass


class ModelControlBox(BaseModel):
    pass


class ModelBoomLength(BaseModel):
    pass


class ModelBoomType(BaseModel):
    pass


class Dealer(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Order(models.Model):
    class Status:
        stock = 'stock'
        payment = 'payment'
        shipment = 'shipment'

    MADE = [
        ('MIF', 'MIF'),
        ('MIR', 'MIR'),
    ]

    STATUS = [
        (Status.stock, Status.stock),
        (Status.payment, Status.payment),
        (Status.shipment, Status.shipment),
    ]

    order = models.PositiveIntegerField()
    berthoud = models.PositiveIntegerField(verbose_name="Berthoud ref. number")
    emc = models.PositiveIntegerField(verbose_name="Emc serial number")
    model_name = models.ForeignKey('ModelName')
    model_control_box = models.ForeignKey('ModelControlBox')
    model_boom_length = models.ForeignKey('ModelBoomLength')
    model_boom_type = models.ForeignKey('ModelBoomType')
    made = models.CharField(max_length=100, choices=MADE)
    ready = models.BooleanField(default=False)
    tested = models.BooleanField(default=False)
    antifreeze = models.BooleanField(default=False)
    description = models.TextField(blank=True)

    status = models.CharField(max_length=100, choices=STATUS, default=Status.stock)

    is_rented = models.BooleanField(default=False)
    renter = models.CharField(max_length=100, blank=True)

    emc_dealer = models.CharField(verbose_name='Specification EMC - Dealer', max_length=255, blank=True)
    dealer = models.ForeignKey('Dealer', blank=True, null=True)
    price = models.PositiveIntegerField(verbose_name='Total price Dealer, EUR', blank=True, null=True)
    payment1 = models.PositiveIntegerField(verbose_name='Payment 1, EUR', blank=True, null=True)
    payment1_date = models.DateField(verbose_name='Payment 1 date', blank=True, null=True)
    payment2 = models.PositiveIntegerField(verbose_name='Payment 2, EUR', blank=True, null=True)
    payment2_date = models.DateField(verbose_name='Payment 2 date', blank=True, null=True)
    payment3 = models.PositiveIntegerField(verbose_name='Payment 3, EUR', blank=True, null=True)
    payment3_date = models.DateField(verbose_name='Payment 3 date', blank=True, null=True)

    shipped_date = models.DateField(verbose_name='shipped the dealer', blank=True, null=True)
    customer = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return '%s %s %s %s' % (self.order, self.berthoud, self.emc, self.model)

    @property
    def model(self):
        return '%s %s %s %s' % (self.model_name, self.model_control_box, self.model_boom_length, self.model_boom_type)

    @property
    def total_payment(self):
        p1 = self.payment1 or 0
        p2 = self.payment2 or 0
        p3 = self.payment3 or 0
        return p1 + p2 + p3


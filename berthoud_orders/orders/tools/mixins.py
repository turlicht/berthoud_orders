# -*- coding: utf-8 -*-
__author__ = 'turlicht'
from django.views.generic.edit import FormMixin
from urllib import urlencode


class FiltersMixin(FormMixin):
    """


    """
    filter_in_get = True  # Использовать ли для фильтров GET-параметры
    filter_prefix = ''

    def form_invalid(self, form):
        pass

    def form_valid(self, form):
        url_filters = {}
        cookie_filters = {}
        for field, value in form.cleaned_data.items():
            if value:
                # Если поле является ссылкой, то берем его id, а не имя
                value = getattr(value, 'id', value)
                url_filters[field] = value
                cookie_filters[field] = value
            else:
                cookie_filters[field] = ''

        if self.filter_in_get and url_filters:
            url_filters = urlencode(url_filters)
            self.success_url += '?' + url_filters

        response = super(FiltersMixin, self).form_valid(form)
        for filter, value in cookie_filters.items():
            response.set_cookie(filter, value)

        return response

    def post(self, request, *args, **kwargs):
        # По сути сохраняем POST запрос в COOKIE
        # и если нужно, дополнительно формируем GET
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            self.form_invalid(form)
            return self.get(self, request, *args, **kwargs)

    def render_to_response(self, context, **kwargs):
        # Сохраним все пришедшие из GET фильтры в COOKIE
        response = super(FiltersMixin, self).render_to_response(context, **kwargs)
        if self.filter_in_get:
            for field in self.form_class.base_fields:
                value = self.request.GET.get(field, '')
                if value:
                    response.set_cookie(field, value)
        return response

    def get_initial(self):
        return self.get_filters()

    def get_filters(self):
        # Вытащим все фильтры из COOKIE, но если есть фильтр в GET, то берем из GET
        filters = {}
        for field in self.form_class.base_fields:
            value = self.request.COOKIES.get(field, '')
            if field in self.request.GET:
                value = self.request.GET.get(field, '')
            if value:
                filters[field] = value
        return filters
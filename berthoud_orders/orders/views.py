# -*- coding: utf-8 -*-
from django.http import Http404
from django.views.generic import ListView, FormView
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormMixin
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import redirect

from tools.mixins import FiltersMixin
from orders.models import Order
from orders.forms import SearchForm


fields_stock = ['order', 'berthoud', 'emc', 'model_name', 'model_control_box', 'model_boom_length', 'model_boom_type',
                'made', 'ready', 'tested', 'antifreeze', 'description']
fields_payment = ['status', 'is_rented', 'renter', 'dealer', 'emc_dealer', 'price',
                  'payment1', 'payment1_date', 'payment2', 'payment2_date', 'payment3', 'payment3_date']
fields_shipment = ['status', 'shipped_date', 'customer']


class CommonMixin(object):
    def get_access(self):
        groups = [group.name for group in self.request.user.groups.all()]

        def in_groups(perms):
            perms.append('manager')
            return any([perm in groups for perm in perms])

        access = {
            'view_stock':   True,
            'view_payment': in_groups(['viewer', 'stock', 'payment', 'shipment']),
            'view_shipment': in_groups(['viewer', 'payment', 'shipment']),
            'update_stock':  in_groups(['stock']),
            'update_payment': in_groups(['payment']),
            'update_shipment': in_groups(['shipment']),
            'create': in_groups(['stock']),
            'delete': in_groups([]),
            'finance': in_groups(['viewer', 'payment']),
        }
        return access

    def dispatch(self, *args, **kwargs):
        # Требуется залогинится
        if not self.request.user.is_authenticated():
            return redirect('login')

        # Проверяем, что пользователь входит в разрешенную группу
        if not self.get_access()[self.perm]:
            return redirect('login')

        return super(CommonMixin, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(CommonMixin, self).get_context_data(**kwargs)

        # Убираем лишние элементы из шаблона
        context['access'] = self.get_access()

        if 'form' in context:
            form = context['form']
            if 'shipped_date' in form.fields:
                form.fields['shipped_date'].widget.attrs = {'class': 'datapicker'}
            if 'payment1_date' in form.fields:
                form.fields['payment1_date'].widget.attrs = {'class': 'datapicker'}
            if 'payment2_date' in form.fields:
                form.fields['payment2_date'].widget.attrs = {'class': 'datapicker'}
            if 'payment3_date' in form.fields:
                form.fields['payment3_date'].widget.attrs = {'class': 'datapicker'}
        return context


class OrdersMixin(FiltersMixin):
    form_class = SearchForm
    status = Order.Status.stock

    def get_context_data(self, **kwargs):
        context = super(OrdersMixin, self).get_context_data(**kwargs)

        # Добавление номера по порядку в заказы
        start = 1
        if 'page_obj' in context:
            start = context['page_obj'].start_index()
        for n, order in enumerate(context['orders'], start):
            order.n = n

        form_class = self.get_form_class()
        form = self.get_form(form_class)
        context['form'] = form

        return context

    def get_queryset(self):
        filters = self.get_filters()
        queryset = self.queryset

        if 'order' in filters:
            queryset = queryset.filter(order=filters['order'])
        if 'berthoud' in filters:
            queryset = queryset.filter(berthoud=filters['berthoud'])
        if 'emc' in filters:
            queryset = queryset.filter(emc=filters['emc'])

        if 'model_name' in filters:
            queryset = queryset.filter(model_name=int(filters['model_name']))
        if 'model_control_box' in filters:
            queryset = queryset.filter(model_control_box=int(filters['model_control_box']))
        if 'model_boom_length' in filters:
            queryset = queryset.filter(model_boom_length=int(filters['model_boom_length']))
        if 'model_boom_type' in filters:
            queryset = queryset.filter(model_boom_type=int(filters['model_boom_type']))
        if 'made' in filters:
            queryset = queryset.filter(made=filters['made'])

        if self.status == Order.Status.stock:
            if 'ready' in filters:
                queryset = queryset.filter(ready=filters['ready'])
            if 'tested' in filters:
                queryset = queryset.filter(tested=filters['tested'])
            if 'antifreeze' in filters:
                queryset = queryset.filter(antifreeze=filters['antifreeze'])
            if 'is_rented' in filters:
                queryset = queryset.filter(is_rented=filters['is_rented'])

        if self.status == Order.Status.shipment:
            if 'shipped_from' in filters:
                queryset = queryset.filter(shipped__gte=filters['shipped_from'])
            if 'shipped_to' in filters:
                queryset = queryset.filter(shipped__lte=filters['shipped_to'])

        queryset = queryset.order_by('id')
        return queryset


class NextMixin(object):

    def _get_next(self, request):
        next = request.POST.get('next', request.GET.get('next', request.META.get('HTTP_REFERER', None)))
        if not next or next == request.path:
            raise Http404  # No next url was supplied in GET or POST.
        return next

    def get_context_data(self, **kwargs):
        context = super(NextMixin, self).get_context_data(**kwargs)
        context['next'] = self._get_next(self.request)
        return context


class OrdersStockView(CommonMixin, OrdersMixin, ListView):
    perm = 'view_stock'
    queryset = Order.objects.filter(status=Order.Status.stock)
    context_object_name = 'orders'
    paginate_by = 20
    success_url = reverse_lazy('orders_stock')
    template_name = 'orders/orders_stock.html'


class OrdersPaymentView(CommonMixin, OrdersMixin, ListView):
    perm = 'view_payment'
    queryset = Order.objects.filter(status__in=[Order.Status.payment, Order.Status.shipment])
    context_object_name = 'orders'
    paginate_by = 20
    success_url = reverse_lazy('orders_payment')
    template_name = 'orders/orders_payment.html'


class OrdersShipmentView(CommonMixin, OrdersMixin, ListView):
    perm = 'view_shipment'
    queryset = Order.objects.filter(status=Order.Status.shipment)
    context_object_name = 'orders'
    paginate_by = 20
    success_url = reverse_lazy('orders_shipment')
    template_name = 'orders/orders_shipment.html'


class OrderCreate(CommonMixin, CreateView):
    perm = 'create'
    model = Order
    template_name = 'orders/order_create.html'
    fields = fields_stock
    success_url = reverse_lazy('orders_stock')


class OrderStockUpdate(CommonMixin, UpdateView):
    perm = 'update_stock'
    queryset = Order.objects.filter(status=Order.Status.stock)
    template_name = 'orders/order_update.html'
    fields = fields_stock
    success_url = reverse_lazy('orders_stock')


class OrderPaymentUpdate(CommonMixin, UpdateView):
    perm = 'update_stock'
    queryset = Order.objects.filter(status=Order.Status.payment)
    template_name = 'orders/order_update.html'
    fields = fields_stock
    success_url = reverse_lazy('orders_payment')


class OrderShipmentUpdate(CommonMixin, UpdateView):
    perm = 'update_stock'
    queryset = Order.objects.filter(status=Order.Status.shipment)
    template_name = 'orders/order_update.html'
    fields = fields_stock
    success_url = reverse_lazy('orders_shipment')


class OrderStockUpdatePayment(CommonMixin, UpdateView):
    perm = 'update_payment'
    queryset = Order.objects.all()
    template_name = 'orders/order_update.html'
    fields = fields_payment
    success_url = reverse_lazy('orders_stock')


class OrderPaymentUpdatePayment(CommonMixin, UpdateView):
    perm = 'update_payment'
    queryset = Order.objects.all()
    template_name = 'orders/order_update.html'
    fields = fields_payment
    success_url = reverse_lazy('orders_payment')


class OrderPaymentUpdateShipment(CommonMixin, UpdateView):
    perm = 'update_shipment'
    queryset = Order.objects.all()
    template_name = 'orders/order_update.html'
    fields = fields_shipment
    success_url = reverse_lazy('orders_payment')


class OrderShipmentUpdatePayment(CommonMixin, UpdateView):
    perm = 'update_payment'
    queryset = Order.objects.all()
    template_name = 'orders/order_update.html'
    fields = fields_payment
    success_url = reverse_lazy('orders_shipment')


class OrderShipmentUpdateShipment(CommonMixin, UpdateView):
    perm = 'update_shipment'
    queryset = Order.objects.all()
    template_name = 'orders/order_update.html'
    fields = fields_shipment
    success_url = reverse_lazy('orders_shipment')


class OrderDeleteStock(CommonMixin, NextMixin, DeleteView):
    perm = 'delete'
    queryset = Order.objects.filter(status=Order.Status.stock)
    success_url = reverse_lazy('orders_stock')


class OrderDeletePayment(CommonMixin, NextMixin, DeleteView):
    perm = 'delete'
    queryset = Order.objects.filter(status=Order.Status.payment)
    success_url = reverse_lazy('orders_payment')


class OrderDeleteShipment(CommonMixin, NextMixin, DeleteView):
    perm = 'delete'
    queryset = Order.objects.filter(status=Order.Status.shipment)
    success_url = reverse_lazy('orders_shipment')

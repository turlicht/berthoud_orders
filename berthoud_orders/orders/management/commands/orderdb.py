from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist

from orders.models import ModelName, ModelControlBox, ModelBoomLength, ModelBoomType, Dealer


class Command(BaseCommand):
    help = 'Init orders app default values'

    def handle(self, *args, **options):
        model_name = ['Tracker 32-34 S', 'Tracker 32-34 NS', 'Raptor 2540', 'Raptor 3240', 'Raptor 4240', 'Raptor 5240']
        for name in model_name:
            ModelName.objects.get_or_create(name=name)

        model_control_box = ['DPTronic', 'ECTronic']
        for name in model_control_box:
            ModelControlBox.objects.get_or_create(name=name)

        model_boom_length = ['18m', '20m', '21m', '24m', '27m', '28m', '30m', '32m', '33m', '36m', '38m', '40m', '42m']
        for name in model_boom_length:
            ModelBoomLength.objects.get_or_create(name=name)

        model_boom_type = ['RLD', 'AXIALE', 'AXIALE II', 'XLTN', 'EKTAR B2', 'EKTAR B3']
        for name in model_boom_type:
            ModelBoomType.objects.get_or_create(name=name)

        dealers = ['test_dealer_1', 'test_dealer_2', 'test_dealer_3']
        for dealer in dealers:
            Dealer.objects.get_or_create(name=dealer)

        groups = ['manager', 'viewer', 'stock', 'payment', 'shipment']
        for name in groups:
            Group.objects.get_or_create(name=name)
            try:
                User.objects.get(username=name)
            except ObjectDoesNotExist:
                User.objects.create_user(name, password=name)

            group = Group.objects.get(name=name)
            user = User.objects.get(username=name)
            user.groups.add(group)


        group_manager = Group.objects.get(name='manager')
        user_admin = User.objects.get(username='admin')
        user_admin.groups.add(group_manager)

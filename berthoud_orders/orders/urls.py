# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from orders.views import OrdersStockView, OrdersPaymentView, OrdersShipmentView, OrderCreate, OrderStockUpdate, \
    OrderPaymentUpdate, OrderShipmentUpdate, \
    OrderStockUpdatePayment, OrderPaymentUpdatePayment, OrderPaymentUpdateShipment, \
    OrderShipmentUpdatePayment, OrderShipmentUpdateShipment, \
    OrderDeleteStock, OrderDeletePayment, OrderDeleteShipment


urlpatterns = patterns('',
    url(r'stock/$', OrdersStockView.as_view(), name='orders_stock'),
    url(r'payment/$', OrdersPaymentView.as_view(), name='orders_payment'),
    url(r'shipment/$', OrdersShipmentView.as_view(), name='orders_shipment'),

    url(r'order/add/$', OrderCreate.as_view(), name='order_create'),

    url(r'order/stock/update/(?P<pk>\d+)/$', OrderStockUpdate.as_view(), name='order_stock_update'),
    url(r'order/payment/update/(?P<pk>\d+)/$', OrderPaymentUpdate.as_view(), name='order_payment_update'),
    url(r'order/shipment/update/(?P<pk>\d+)/$', OrderShipmentUpdate.as_view(), name='order_shipment_update'),

    url(r'order/stock/update/payment/(?P<pk>\d+)/$', OrderStockUpdatePayment.as_view(), name='order_stock_update_payment'),
    url(r'order/payment/update/payment/(?P<pk>\d+)/$', OrderPaymentUpdatePayment.as_view(), name='order_payment_update_payment'),
    url(r'order/payment/update/shipment/(?P<pk>\d+)/$', OrderPaymentUpdateShipment.as_view(), name='order_payment_update_shipment'),
    url(r'order/shipment/update/payment/(?P<pk>\d+)/$', OrderShipmentUpdatePayment.as_view(), name='order_shipment_update_payment'),
    url(r'order/shipment/update/shipment/(?P<pk>\d+)/$', OrderShipmentUpdateShipment.as_view(), name='order_shipment_update_shipment'),


    url(r'order/stock/delete/(?P<pk>\d+)/$', OrderDeleteStock.as_view(), name='order_stock_delete'),
    url(r'order/payment/delete/(?P<pk>\d+)/$', OrderDeletePayment.as_view(), name='order_payment_delete'),
    url(r'order/shipment/delete/(?P<pk>\d+)/$', OrderDeleteShipment.as_view(), name='order_shipment_delete'),
)
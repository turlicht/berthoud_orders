from django.contrib import admin
from models import Order, ModelName, ModelControlBox, ModelBoomLength, ModelBoomType, Dealer


admin.site.register([Order, ModelName, ModelControlBox, ModelBoomLength, ModelBoomType, Dealer])

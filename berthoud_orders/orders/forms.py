# -*- coding: utf-8 -*-
from django import forms
from django.forms import ModelChoiceField

from orders.models import Order, ModelName, ModelControlBox, ModelBoomLength, ModelBoomType


class ModelField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class SearchForm(forms.Form):
    MADE = [('', '---------')] + Order.MADE

    order = forms.CharField(required=False)
    berthoud = forms.CharField(required=False)
    emc = forms.CharField(required=False)

    model_name = ModelField(queryset=ModelName.objects, required=False)
    model_control_box = ModelField(queryset=ModelControlBox.objects, required=False)
    model_boom_length = ModelField(queryset=ModelBoomLength.objects, required=False)
    model_boom_type = ModelField(queryset=ModelBoomType.objects, required=False)

    made = forms.ChoiceField(choices=MADE, required=False)
    ready = forms.BooleanField(required=False)
    tested = forms.BooleanField(required=False)
    antifreeze = forms.BooleanField(required=False)
    is_rented = forms.BooleanField(required=False)

    shipped_from = forms.DateField(required=False,
                                   widget=forms.DateInput(attrs={'class': 'datapicker'}))
    shipped_to = forms.DateField(required=False,
                                   widget=forms.DateInput(attrs={'class': 'datapicker'}))